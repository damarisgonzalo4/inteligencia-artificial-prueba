biggest(elephant, horse).
biggest(horse, donkey).
biggest(donkey, dog).
biggest(donkey, simp).
biggest(simp, industrial).
biggest(dog, ant).

%Rules
is_biggest(X, Y) :- biggest(X, Y).
is_biggest(X, Y) :- biggest(X, Z), is_biggest(Z, Y).
