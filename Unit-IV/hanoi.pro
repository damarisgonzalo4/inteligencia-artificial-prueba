move_disk(1, ORIGIN, DESTINATION, _) :-
    write(ORIGIN),
    write(" to "),
    write(DESTINATION),
    nl.

move_disk(DISKS_NUMBER, ORIGIN, DESTINATION, X) :-
    DISKS_NUMBER > 1,
    NEW_DISKS_NUMBER is DISKS_NUMBER - 1,
    move_disk(NEW_DISKS_NUMBER, ORIGIN, X, DESTINATION),
    move_disk(1, ORIGIN, DESTINATION, _),
    move_disk(NEW_DISKS_NUMBER, X, DESTINATION, ORIGIN).
