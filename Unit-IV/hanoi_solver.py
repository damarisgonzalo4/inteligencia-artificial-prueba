from pyswip import Prolog


interpreter = Prolog()
interpreter.consult("hanoi.pl")

solution = interpreter.query("move_disk(3 ,1, 3, 2)")

for move in solution:
    print(move)
