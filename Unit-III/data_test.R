data(women)

# Build in linear model
model = lm(weight~height, data=women)
#model

# Prediction
predict = -87.52 + 3.45 * women$height
